The Repo includes:

A presentation file.
A sample text files with queries to run on server.
A sample Python file, to show the demo of "py2neo" package.


Required Installations:

//Installing server to run the queries on localhost
-Neo4j community server from: 
		http://www.neo4j.org/download

//Installing package for running the "Py2neo_sample.py"
-Installing py2neo package:
		pip install py2neo