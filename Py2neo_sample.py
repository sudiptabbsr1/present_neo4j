from py2neo import neo4j, cypher

graph_db = neo4j.GraphDatabaseService()
#a, b, ab = graph_db.create(node(name="Alice"), node(name="Bob"), rel(0, "KNOWS", 1))
query = neo4j.CypherQuery(graph_db, "CREATE (a {name:{name_a}})-[ab:DEMOS]->(b {name:{name_b}})"
                              "RETURN a, b, ab")
a, b, ab = query.execute(name_a="Sudipta", name_b="Neo4j").data[0]